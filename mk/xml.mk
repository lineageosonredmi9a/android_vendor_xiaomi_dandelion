PRODUCT_PACKAGES += \
    android.hardware.gpu@1.0-service.xml \
    android.hardware.wifi.hostapd.xml \
    android.hardware.wifi@1.0-service.xml \
    lights-mtk-default.xml \
    manifest.xml \
    manifest_android.hardware.drm@1.3-service.widevine.xml \
    manifest_vendor.xiaomi.hardware.mlipay.xml \
    manifest_vendor.xiaomi.hardware.mtdservice.xml \
    manifest_vendor.xiaomi.hardware.vsimapp.xml \
    power-default.xml \
    vendor.mediatek.hardware.dplanner@2.0-service.xml \
    vendor.xiaomi.hardware.misys@1.0.xml \
    vendor.xiaomi.hardware.misys@2.0.xml \
    vendor.xiaomi.hardware.misys@3.0.xml \
    vibrator-mtk-default.xml \
    manifest_dsds.xml \
    manifest_qsqs.xml \
    manifest_ss.xml \
    manifest_tsts.xml

