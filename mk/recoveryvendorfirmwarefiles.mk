# recovery vendor firmware files
PRODUCT_COPY_FILES += \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/BT_FW.cfg:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/BT_FW.cfg \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/Conf_MultipleTest_aa.ini:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/Conf_MultipleTest_aa.ini \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/Conf_MultipleTest_ab.ini:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/Conf_MultipleTest_ab.ini \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/Himax_firmware.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/Himax_firmware.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/Himax_mpfw.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/Himax_mpfw.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/WIFI_RAM_CODE_soc1_0_1_1.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/WIFI_RAM_CODE_soc1_0_1_1.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/WMT_SOC.cfg:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/WMT_SOC.cfg \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/chipone-tddi-tianma.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/chipone-tddi-tianma.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/chipone-tddi-truly.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/chipone-tddi-truly.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/fm_cust.cfg:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/fm_cust.cfg \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/focaltech_aa_ts_fw_helitai.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/focaltech_aa_ts_fw_helitai.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/focaltech_ab_ts_fw_helitai.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/focaltech_ab_ts_fw_helitai.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/gt1151_default_firmware2.img:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/gt1151_default_firmware2.img \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/hx_criteria.csv:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/hx_criteria.csv \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/ilitek_fw.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/ilitek_fw.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/mt6631_fm_v1_coeff.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/mt6631_fm_v1_coeff.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/mt6631_fm_v1_patch.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/mt6631_fm_v1_patch.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/novatek_ts_djn_fw.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/novatek_ts_djn_fw.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/novatek_ts_djn_mp.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/novatek_ts_djn_mp.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/novatek_ts_ebbg_fw.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/novatek_ts_ebbg_fw.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/novatek_ts_ebbg_mp.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/novatek_ts_ebbg_mp.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/novatek_ts_hlt_fw.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/novatek_ts_hlt_fw.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/novatek_ts_hlt_mp.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/novatek_ts_hlt_mp.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/novatek_ts_truly_fw.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/novatek_ts_truly_fw.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/novatek_ts_truly_mp.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/novatek_ts_truly_mp.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/rgx.fw:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/rgx.fw \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/rgx.sh:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/rgx.sh \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/soc1_0_patch_mcu_1_1_hdr.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/soc1_0_patch_mcu_1_1_hdr.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/soc1_0_ram_bt_1_1_hdr.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/soc1_0_ram_bt_1_1_hdr.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/soc1_0_ram_mcu_1_1_hdr.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/soc1_0_ram_mcu_1_1_hdr.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/soc1_0_ram_wifi_1_1_hdr.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/soc1_0_ram_wifi_1_1_hdr.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/soc3_0_ram_bt_1_1_hdr.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/soc3_0_ram_bt_1_1_hdr.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/soc3_0_ram_bt_1a_1_hdr.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/soc3_0_ram_bt_1a_1_hdr.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/soc3_0_ram_mcu_1_1_hdr.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/soc3_0_ram_mcu_1_1_hdr.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/soc3_0_ram_mcu_1a_1_hdr.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/soc3_0_ram_mcu_1a_1_hdr.bin \
    $(VENDOR_PATH)/proprietary/recovery/root/vendor/firmware/soc3_0_ram_mcu_e1_hdr.bin:$(TARGET_COPY_OUT_RECOVERY)/root/vendor/firmware/soc3_0_ram_mcu_e1_hdr.bin

